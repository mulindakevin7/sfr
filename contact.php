<?php

require('inc/pdo.php');
require('inc/function.php');
//treat PHP
$errors = array();
$triumph = false;
//if formulair submitted

if (!empty($_POST['submitted'])){
    //Xss
    $mail = clearXss('mail');
    $message = clearXss('message');

    //validation
    $errors = validationContact($errors,$mail,'mail',1,150);
    $errors = validationContact($errors,$message,'message',5,2000);

    //if no errors
    if (count($errors) == 0){
        //insert into
        $sql="INSERT INTO contacter (Mail, Message, Created_at) VALUES (:mail, :message, NOW())";
        $query= $pdo->prepare($sql);
        $query->bindValue('mail',$mail);
        $query->bindValue('message', nl2br($message));
        $query->execute();
        $triumph=true;

    }

}

?>
<?php if ($triumph == true){
    echo '<h1>Merci!</h1>';
}else{?>

    <!doctype html>
    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>Document</title>
        <link rel="stylesheet" href="asset\css\style.css">
    </head>
    <body>
    <header>
        <nav class="wrap2">
            <ul>
                <li><a href="index.php"><img src="asset\images\logo-sfr.svg" alt="logo_sfr" width="20%"></a></li>
                <li><a href="listing.php"><img src="asset\images\logo-menu1.svg" alt="" width="20%"></a>les profils</li>
                <li><a href=""><img src="asset\images\logo-menu2.svg" alt="" width="10%"></a>les offres par région</li>
            </ul>
        </nav>
    </header>
    <div id="image">
        <img src="asset\images\image-fond.jpg" alt="image avec un homme decu" width="100%">
    </div>
    <section class="Wrap2 formulaire">
        <form action="" method="post" novalidate>
            <input type="text" name="mail" id="mail" placeholder="mail" value="<?php if (!empty($_POST['mail'])){echo $_POST['mail'];} ?>">
            <span class="error"><?php if (!empty($errors['mail'])){echo $errors['mail'];}?></span>
            <textarea name="message" id="message" cols="30" rows="10" placeholder="message"><?php if (!empty($_POST['message'])){echo $_POST['message'];} ?></textarea>
            <span class="error"><?php if (!empty($errors['message'])){echo $errors['message'];}?></span>
            <input type="submit" name="submitted" value="Envoyer">

        </form>
    </section>

    </body>
    </html>
    <?php }?>

<?php include('inc/footer.php');











